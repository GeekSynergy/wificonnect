const readline = require('readline');
var piWifi = require('pi-wifi');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


piWifi.listNetworks(function(err, networksArray) {
  if (err) {
    return console.error(err.message);
  }
  console.log(networksArray);
});

piWifi.scan(function(err, networks) {
  if (err) {
    return console.error(err.message);
  }
  console.log(networks);
});

 
piWifi.check('starya', function(err, result) {
  if (err) {
    return console.error(err.message);
  }
  console.log(result);
});
 
// => If everything is working
// { selected: true, connected: true, ip: '192.168.0.12' }
// => Alternatively
// { selected: false, connected: false }

rl.question('What\'s the Password? ', (answer) => {
  // TODO: Log the answer in a database
  console.log(`You have entered password : ${answer}`);
  piWifi.connect('PhoenixFrames', 'p@ssw0rd', function(err) {
  if (err) {
    return console.error(err.message);
  }
  console.log('Successful connection to PhoenixFrames!');
  });

  rl.close();
});
